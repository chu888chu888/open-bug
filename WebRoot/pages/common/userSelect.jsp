<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<div id="userModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">选择用户</h3>
	</div>
	
	<div class="modal-body">
		<table id="usersTab" class="list-table">
			<tr>
				<th width="80px"></th>
				<th width="310px">用户名</th>
				<th width="150px">职位</th>
			</tr>
		</table>
	</div>
	
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
		<button onclick="selectUser(true);" class="btn">清空</button>
		<button onclick="selectUser();" class="btn btn-primary">确认</button>
	</div>
</div>

<script>
	var $_u_src;
	
	function loadUserData($inp) {
		$_u_src = $inp;
		var id = $inp.attr("val"), id = id ? id : "";
		
		_remoteCall("user/getUsers.do", {type: 0}, function(data) {
			var info = eval(data), html = "", $tab = $("#usersTab");
			
			// remove origin rows
			$("tr:gt(0)", $tab).remove();
			
			for(var i in info) {
				html += "<tr id='" + info[i].id + "' onclick='clickUser($(this));' ondblclick='selectUser();'>";
				html += "<td class='sn'><input name='_user' type='radio' " + (id == info[i].id ? "checked" : "") + " /></td>";
				html += "<td><div>" + info[i].username + "</div></td>";
				html += "<td><div>" + combinePosition(info[i].type) + "</div></td>";
				html += "</tr>";
			}
			
			$tab.append(html);
			
			// init radio box
			$("input").iCheck({radioClass: "iradio_square-blue"});
		});
	}
	
	function clickUser($tr) {
		$("input", $tr).iCheck("check");
	}
	
	function selectUser(isClear) {
		if(isClear) {
			$_u_src.attr("val", "").val("");
		} else {
			var $sel = $("input:radio:checked").closest("tr");
			$_u_src.attr("val", $sel.attr("id")).val($("td:eq(1)", $sel).text());
		}
		
		// if has filter, call search to refresh table
		if(typeof(search) == "function")  search();
		
		$("#userModal").modal("hide");
	}
	
	function combinePosition(t) {
		if(t == 1) {
			return "项目经理";
		} else if(t == 2) {
			return "研发工程师";
		} else {
			return "测试工程师";
		}
	}
</script>
