<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cloud" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE HTML PUBtdC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/jquery.fancybox.css" />" />
	<script type="text/javascript" src="<c:url value="/scripts/jquery.fancybox.pack.js" />"></script>
</head>

<body>
	<div class="wrapper">
	
		<div id="btnDiv">
			<c:if test="${param.workspace == 'Y'}">
				<a href="<c:url value="/work/openWork.do" />" class="btn">返回</a>
			</c:if>
			<c:if test="${param.workspace != 'Y'}">
				<a href="<c:url value="/pages/bug/bug.jsp" />" class="btn">返回</a>
			</c:if>
			
			<a href="<c:url value="/bug/openOperate.do?op=gotest&fromStatus=3&bugId=${param.bugId}" />" class="btn">提交测试</a>
			<a href="<c:url value="/bug/openOperate.do?op=assign&fromStatus=3&bugId=${param.bugId}" />" class="btn">更换责任人</a>
			<a href="<c:url value="/bug/openOperate.do?op=hangup&fromStatus=3&bugId=${param.bugId}" />" class="btn">挂起</a>
		</div>
		
		<div class="name">${bug.name}</div>
		
		<table class="detail-table">
			<tr>
				<td class="label-td" width="100px">项目状态</td><td width="250px">${bug.statusName}</td>
				<td class="label-td" width="100px">所属项目</td><td width="250px">${bug.projectName}</td>
			</tr>
			<tr class="odd">
				<td class="label-td">责任人</td><td>${bug.owner}</td>
				<td class="label-td">修改人</td><td><cloud:user userId="${bug.modifierId}" /></td>
			</tr>
			<tr>
				<td class="label-td">缺陷描述</td><td colspan="3">${bug.intro}</td>
			</tr>
			<tr class="odd">
				<td class="label-td">重现步骤</td><td colspan="3">${bug.reappear}</td>
			</tr>
			<tr>
				<td class="label-td">创建时间</td><td><cloud:time date="${bug.createTime}" /></td>
				<td class="label-td">最近更新时间</td><td><cloud:time date="${bug.modifyTime}" /></td>
			</tr>
		</table>
		
		<cloud:attach attachs="${attachs}" />
		
		<cloud:record records="${records}" />
		
		<cloud:workflow status="${bug.status}" records="${records}" />
	</div>
</body>
</html>
