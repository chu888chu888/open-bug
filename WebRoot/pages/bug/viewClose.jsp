<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cloud" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE HTML PUBtdC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/jquery.fancybox.css" />" />
	<script type="text/javascript" src="<c:url value="/scripts/jquery.fancybox.pack.js" />"></script>
</head>

<body>
	<div class="wrapper">
	
		<div id="btnDiv">
			<c:if test="${param.workspace == 'Y'}">
				<a href="<c:url value="/work/openWork.do" />" class="btn">返回</a>
			</c:if>
			<c:if test="${param.workspace != 'Y'}">
				<a href="<c:url value="/pages/bug/bug.jsp" />" class="btn">返回</a>
			</c:if>
			
			<a href="<c:url value="/bug/openOperate.do?op=reopen&fromStatus=6&bugId=${param.bugId}" />" class="btn">重新开启</a>
			<input type="button" class="btn" value="删除" onclick="confirmRemove();" />
		</div>
		
		<div class="name">${bug.name}</div>
		
		<table class="detail-table">
			<tr>
				<td class="label-td" width="100px">项目状态</td><td width="250px">${bug.statusName}</td>
				<td class="label-td" width="100px">所属项目</td><td width="250px">${bug.projectName}</td>
			</tr>
			<tr class="odd">
				<td class="label-td">创建人</td><td><cloud:user userId="${bug.creatorId}" /></td>
				<td class="label-td">审核人</td><td><cloud:user userId="${bug.auditorId}" /></td>
			</tr>
			<tr>
				<td class="label-td">修改人</td><td><cloud:user userId="${bug.modifierId}" /></td>
				<td class="label-td">测试人</td><td><cloud:user userId="${bug.testorId}" /></td>
			</tr>
			<tr class="odd">
				<td class="label-td">缺陷描述</td><td colspan="3">${bug.intro}</td>
			</tr>
			<tr>
				<td class="label-td">重现步骤</td><td colspan="3">${bug.reappear}</td>
			</tr>
			<tr class="odd">
				<td class="label-td">解决方法</td><td colspan="3">${bug.solveInfo}</td>
			</tr>
			<tr>
				<td class="label-td">关联测试</td><td colspan="3">${bug.relateTest}</td>
			</tr>
			<tr class="odd">
				<td class="label-td">创建时间</td><td><cloud:time date="${bug.createTime}" /></td>
				<td class="label-td">最近更新时间</td><td><cloud:time date="${bug.modifyTime}" /></td>
			</tr>
		</table>
		
		<cloud:attach attachs="${attachs}" />
		
		<cloud:record records="${records}" />
		
		<cloud:workflow status="${bug.status}" records="${records}" />
	</div>
	
	<div id="dialog-confirm" class="hide" title="缺陷删除">
		<p style="padding: 5px;"><i class="icon-trash" style="margin-right: 15px;"></i></span>确定要从缺陷库中删除该缺陷么？</p>
	</div>

	<script>
		function confirmRemove() {
			$( "#dialog-confirm" ).dialog({
				resizable: false,
			    height: 200,
			    modal: true,
			    buttons: {
			        "確定": function() {
			            location.href = parent.basePath + "bug/removeBug.do?bugId=${param.bugId}";
			        },
			        "取消": function() {
			            $(this).dialog("close");
			    	}
				}
			});
		}
	</script>
</body>
</html>
