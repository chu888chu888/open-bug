<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cloud" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<style>
		.input-text {width: 460px;}
		textarea {width: 460px;height: 150px;}
		.mid-td {padding-left: 5px;padding-bottom: 8px;font-weight: bold;}
	</style>
</head>

<body>
	<div class="wrapper">
	
		<form action="<c:url value="/bug/saveView.do" />" onsubmit="return initForm();" method="post">
		
		<div id="btnDiv">
			<input type="submit" class="btn" value="保存" />
			<a href="<c:url value="/pages/bug/bug.jsp" />" class="btn">返回</a>
			
			<c:if test="${param.viewId != null && param.viewId != ''}">
				<a href="#" class="btn" onclick="removeView();return false;">删除</a>
			</c:if>
		</div>
		
		<div class="title">基础配置</div>
		
		<table class="edit-table" style="margin-left: 30px;">
			<tr>
				<td class="left-td" width="80px">视图名称</td>
				<td><input name="name" type="text" class="input-text input-require" value="${view.name}" style="width: 515px;" /></td>
			</tr>
		</table>
		
		<div class="title">分页配置</div>
		
		<table class="edit-table" style="margin-left: 30px;">
			<tr>
				<td class="left-td" width="80px">单页缺陷数</td>
				<td><input name="pageSize" type="text" class="input-text" value="${view.pageSize}" style="width: 515px;" /></td>
			</tr>
		</table>
		
		<div class="title">过滤配置</div>
		
		<table class="edit-table" style="margin-left: 30px;">
			<tr>
				<td class="left-td" width="80px">缺陷名称</td>
				<td class="mid-td" width="50px">like</td>
				<td><input name="bugName" type="text" class="input-text" value="${view.bugName}" /></td>
			</tr>
			<tr>
				<td class="left-td">所属项目</td>
				<td class="mid-td">in</td>
				<td><input id="projectIds" type="text" class="input-text" ondblclick="showProjects($(this));" onkeydown="return false;" val="${view.projectIds}" value="<cloud:projects ids="${view.projectIds}" />" /><i onclick="$(this).prev().dblclick();" class="icon-search input-icon"></i></td>
			</tr>
			<tr>
				<td class="left-td">状态</td>
				<td class="mid-td">in</td>
				<td><input id="status" type="text" class="input-text" ondblclick="showStatuses($(this));" onkeydown="return false;" val="${view.status}" value="<cloud:statuses ids="${view.status}" />" /><i onclick="$(this).prev().dblclick();" class="icon-search input-icon"></i></td>
			</tr>
			<tr>
				<td class="left-td">责任人</td>
				<td class="mid-td">in</td>
				<td><input id="ownerIds" type="text" class="input-text" ondblclick="showUsers($(this));" onkeydown="return false;" val="${view.ownerIds}" value="<cloud:users ids="${view.ownerIds}" />" /><i onclick="$(this).prev().dblclick();" class="icon-search input-icon"></i></td>
			</tr>
		</table>
		
		<input type="hidden" id="viewId" name="id" value="${view.id}" />
		<input type="hidden" id="_projectIds" name="projectIds" />
		<input type="hidden" id="_status" name="status" />
		<input type="hidden" id="_ownerIds" name="ownerIds" />
		
		</form>
	</div>
	
	<div id="dialog-confirm" title="删除视图">
	  <p>确定要删除此视图么？</p>
	</div>
	
	<jsp:include page="/pages/common/projectSelectMul.jsp"></jsp:include>
	<jsp:include page="/pages/common/userSelectMul.jsp"></jsp:include>
	<jsp:include page="/pages/common/statusSelectMul.jsp"></jsp:include>
	
	<script>
		function initForm() {
			$("#_projectIds").val($("#projectIds").attr("val") ? $("#projectIds").attr("val") : "");
			$("#_status").val($("#status").attr("val") ? $("#status").attr("val") : "");
			$("#_ownerIds").val($("#ownerIds").attr("val") ? $("#ownerIds").attr("val") : "");
			
			return checkForm();
		}
		
		function removeView() {
			$( "#dialog-confirm" ).dialog({
			      height: 180,
			      modal: true,
			      buttons: {
			        "删除": function() {
			        	location.href = parent.basePath + "bug/removeView.do?viewId=" + $("#viewId").val();
			            $(this).dialog("close");
			        },
			        "取消": function() {
			            $(this).dialog( "close" );
			        }
			      }
			});
		}
	</script>
</body>
</html>
