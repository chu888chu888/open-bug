/**
 * 调用后台SpringMVC Action
 */
function _remoteCall(url, params, callback) {
	$.post(parent.basePath + url, params ? params : {}, function(data) {
		if(typeof(callback) == "function")  callback(data);		
	});
}

/**
 * open wait loader
 * 
 * @param type
 */
function openLoader(type) {
	if(!type)  type = 1;
	$("#loaderTip", top.document).text($("#loaderTip" + type, top.document).text());
	
	$("#loader", top.document).show();
}

/**
 * close wait loader
 */
function closeLoader() {
	$("#loader", top.document).hide();
}

/**
 * show select users dialog
 * 
 * @param $inp
 */
function showUsers($inp) {
	loadUserData($inp);
	$("#userModal").modal("show");
}

/**
 * show select project dialog
 * 
 * @param $inp
 */
function showProjects($inp) {
	loadProjectData($inp);
	$("#projectModal").modal("show");
}

/**
 * show select status dialog
 * 
 * @param $imp
 */
function showStatuses($inp) {
	init($inp);
	$("#statusModal").modal("show");
}

/**
 * ****************************************************************************
 * date operates
 * ****************************************************************************
 */
function getDateStr(date) {
	if(!date)  return "";
	
	// if is java date json, convert to javascript date
	if(date.time)  date = new Date(date.time);
	
	return date.getFullYear() + "-" + formatTime(date.getMonth() + 1) + "-" + formatTime(date.getDate());
}

function getTimeStr(date, isSimple) {
	if(!date)  return "";
	
	// if is java date json, convert to javascript date
	if(date.time)  date = new Date(date.time);
	
	return (isSimple ? "" : (date.getFullYear() + "-")) + formatTime(date.getMonth() + 1) + "-" + formatTime(date.getDate())
			+ " " + formatTime(date.getHours()) + ":" + formatTime(date.getMinutes());
}

function formatTime(d) {
	if(d < 10)  return "0" + d;
	return d;
}

/**
 * ****************************************************************************
 * page operates
 * ****************************************************************************
 */
function initPage(page) {
	var html = "<li" + (page.page == 1 ? " class='disabled'" : "") + " onclick='gotoPage($(this), -1, " + page.page + ");'><a href='#' onclick='return false;'>上一页</a></li>";
	
	for(var i = 0; i < page.pageNum; i++) {
		html += "<li" + (page.page == (parseInt(i) + 1) ? " class='disabled'" : "") + " onclick='gotoPage($(this));'><a href='#' onclick='return false;'>" + (parseInt(i) + 1) + "</a></li>";
	}
	
	html += "<li" + (page.page == page.pageNum ? " class='disabled'" : "") + " onclick='gotoPage($(this), 1, " + page.page + ");'><a href='#' onclick='return false;'>下一页</a></li>";
	
	$("div.pagination ul").html(html);
}

function gotoPage($li, next, page) {
	if($li.hasClass("disabled"))  return;
	if(typeof(search) != "function")  return;
	
	if(!next) {
		search($li.text().trim());
	}
	else if(next < 0) {
		search(page - 1);
	}
	else if (next > 0) {
		search(page + 1);
	}
}

function getPageSn(page, i) {
	return ((page.page - 1) * page.pageSize + parseInt(i) + 1);
}