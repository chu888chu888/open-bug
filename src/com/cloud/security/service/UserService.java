package com.cloud.security.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloud.platform.Constants;
import com.cloud.platform.IDao;
import com.cloud.platform.StringUtil;
import com.cloud.security.model.User;

@Service
public class UserService {

	@Autowired
	private IDao dao;
	
	/**
	 * remove user
	 * 
	 * @param userId
	 */
	public void removeUser(String userId) {
		
		if(StringUtil.isNullOrEmpty(userId)) {
			return;
		}
		
		User u = getUserById(userId);
		u.setIsValid(Constants.VALID_NO);
		
		dao.saveObject(u);
	}
	
	/**
	 * change password
	 * 
	 * @param userId
	 * @param newPassword
	 */
	public void changePwd(String userId, String newPassword) {
		
		if(StringUtil.isNullOrEmpty(userId, newPassword)) {
			return;
		}
		
		User u = getUserById(userId);
		u.setPassword(newPassword);
		
		dao.saveObject(u);
	}
	
	/**
	 * get user by user id
	 * 
	 * @param userId
	 * @return
	 */
	public User getUserById(String userId) {
		
		if(StringUtil.isNullOrEmpty(userId)) {
			return new User();
		}
		
		return (User) dao.getObject(User.class, userId);
	}
	
	/**
	 * search type users
	 * 
	 * @param type
	 * @return
	 */
	public List<User> searchUsers(int type) {
		String hql = "";
		
		// select all users
		if(type == Constants.USER_TYPE_ALL) {
			hql = "from User where username != 'admin' and (isValid is null or isValid != 'N') order by username";
			return dao.getAllByHql(hql);
		}
		// select type users
		else {
			hql = "from User where type = ? and username != 'admin' and (isValid is null or isValid != 'N') order by username";
			return dao.getAllByHql(hql, type);
		}
	}
	
	/**
	 * save user
	 * 
	 * @param username
	 * @param password
	 * @param email
	 * @param address
	 */
	public void saveUser(User user) {
		
		if(StringUtil.isNullOrEmpty(user.getId())) {
			user.setId(Constants.getID());
		}
		
		user.setIsValid(Constants.VALID_YES);
		dao.saveObject(user);
		
		// reset user cache
		Constants.userIdNameMap = null;
		Constants.userNameIdMap = null;
	}
}
