package com.cloud.bug.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.cloud.bug.model.Bug;
import com.cloud.bug.model.PageField;
import com.cloud.platform.Constants;

public class BugPageUtil {
	
	/**
	 * operate types
	 */
	public static final String OP_CREATE = "create";
	public static final String OP_EDIT = "edit";
	public static final String OP_COMMIT = "commit";
	public static final String OP_ASSIGN = "assign";
	public static final String OP_UNPASS = "unpass";
	public static final String OP_HANGUP = "hangup";
	public static final String OP_CLOSE = "close";
	public static final String OP_GOTEST = "gotest";
	public static final String OP_SUCCESS = "success";
	public static final String OP_FAILURE = "fail";
	public static final String OP_CHGTESTOR = "chgtestor";
	public static final String OP_REOPEN = "reopen";
	
	/**
	 * html types
	 */
	public static final String HTML_TEXT = "TEXT";
	public static final String HTML_PROJECT = "PROJECT";
	public static final String HTML_USER = "USER";
	public static final String HTML_TEXTAREA = "TEXTAREA";
	public static final String HTML_ATTACH = "attach";
	public static final String HTML_SELECT = "SELECT";
	
	/**
	 * page fields
	 */
	public static PageField field_name;
	public static PageField field_project;
	public static PageField field_auditor;
	public static PageField field_intro;
	public static PageField field_reappear;
	public static PageField field_note;
	public static PageField field_modifier;
	public static PageField field_testorId;
	public static PageField field_solveinfo;
	public static PageField field_relatetest;
	public static PageField field_attach;
	public static PageField field_level;
	public static PageField field_priority;
	
	static {
		field_name = new PageField("name", "缺陷名称", HTML_TEXT);
		field_project = new PageField("projectId", "所属项目", HTML_PROJECT);
		field_auditor = new PageField("auditorId", "审核人", HTML_USER);
		field_intro = new PageField("intro", "缺陷描述", HTML_TEXTAREA);
		field_reappear = new PageField("reappear", "重现步骤", HTML_TEXTAREA);
		field_note = new PageField("note", "备注", HTML_TEXTAREA);
		field_modifier = new PageField("modifierId", "修改人", HTML_USER);
		field_testorId = new PageField("testorId", "测试人", HTML_USER);
		field_solveinfo = new PageField("solveInfo", "解决方法", HTML_TEXTAREA);
		field_relatetest = new PageField("relateTest", "关联测试", HTML_TEXTAREA);
		field_attach = new PageField("attach", "附件", HTML_ATTACH);
		field_level = new PageField("level", "严重性", HTML_SELECT);
		field_priority = new PageField("priority", "优先级", HTML_SELECT);
	}

	/**
	 * combine bug oeprate field html
	 * 
	 * @param operate
	 * @param bug
	 * @return
	 * @throws Exception 
	 */
	public static String combineFieldHtml(String operate, Bug bug) throws Exception {
		
		StringBuffer sb = new StringBuffer();
		sb.append("<table class='edit-table'>");
		
		List<PageField> pageFields = getPageFields(operate);
		
		for(PageField f : pageFields) {
			sb.append(fieldHtml(f, bug));
		}
		
		sb.append("</table>");
		return sb.toString();
	}
	
	/**
	 * init field html by field html type
	 * 
	 * @param field
	 * @return
	 * @throws Exception 
	 */
	private static String fieldHtml(PageField field, Bug bug) throws Exception {
		StringBuffer sb = new StringBuffer();
		
		// get field value
		String value = "";
		
		if(!HTML_ATTACH.equals(field.getHtmlType())) {
			String methodName = "get" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
			Method fieldMethod = Bug.class.getMethod(methodName);
			Object v = fieldMethod.invoke(bug);
			value = v == null ? "" : String.valueOf(value);
		}
		
		// combine html
		sb.append("<tr>");
		sb.append("<td class='left-td' width='80px'>" + field.getLabel() + "</td>");
		sb.append("<td>");
		
		// text field
		if(HTML_TEXT.equals(field.getHtmlType())) {
			sb.append("<input name='" + field.getName() + "' value='" + value + "' type='text' class='input-text " + (field.isRequired() ? "input-require" : "") + "' />");
		}
		// project field
		if(HTML_PROJECT.equals(field.getHtmlType())) {
			sb.append("<input id='" + field.getName() + "' field='relate' type='text' class='input-text " + (field.isRequired() ? "input-require" : "") + "'");
			sb.append(" ondblclick='showProjects($(this));' onkeydown='return false;'");
			sb.append(" val='" + value + "' value='" + Constants.getProjectNameById(value) + "' />");
			sb.append("<i onclick='$(this).prev().dblclick();' class='icon-search input-icon'></i>");
			
			sb.append("<input id='_" + field.getName() + "' type='hidden' name='" + field.getName() + "' />");
		}
		// user field
		if(HTML_USER.equals(field.getHtmlType())) {
			sb.append("<input id='" + field.getName() + "' field='relate' type='text' class='input-text " + (field.isRequired() ? "input-require" : "") + "'");
			sb.append(" ondblclick='showUsers($(this));' onkeydown='return false;'");
			sb.append(" val='" + value + "' value='" + Constants.getUsernameById(value) + "' />");
			sb.append("<i onclick='$(this).prev().dblclick();' class='icon-search input-icon'></i>");
			
			sb.append("<input id='_" + field.getName() + "' type='hidden' name='" + field.getName() + "' />");
		}
		// textarea field
		if(HTML_TEXTAREA.equals(field.getHtmlType())) {
			sb.append("<textarea name='" + field.getName() + "' " + (field.isRequired() ? "class='input-require'" : "") + ">" + value + "</textarea>");
		}
		// select field
		if(HTML_SELECT.equals(field.getHtmlType())) {
			sb.append("<input name='" + field.getName() + "' value='" + value + "' type='text' class='input-text " + (field.isRequired() ? "input-require" : "") + "' />");
		}
		
		sb.append("</td>");
		sb.append("</tr>");
		
		return sb.toString();
	}
	
	/**
	 * get page fields
	 * 
	 * @param operate
	 * @param bug
	 * @return
	 */
	public static List<PageField> getPageFields(String operate) {
		
		List<PageField> pageFields = new ArrayList();
		
		if(OP_CREATE.equals(operate) || OP_EDIT.equals(operate) || OP_COMMIT.equals(operate)) {
			pageFields.add(field_name);
			pageFields.add(field_project);
			pageFields.add(field_auditor);
			pageFields.add(field_level);
			pageFields.add(field_priority);
			pageFields.add(field_intro);
			pageFields.add(field_reappear);
			field_name.setRequired(true);
			field_project.setRequired(true);
			field_auditor.setRequired(true);
		}
		else if(OP_UNPASS.equals(operate) || OP_HANGUP.equals(operate) || OP_CLOSE.equals(operate) || OP_SUCCESS.equals(operate) || OP_REOPEN.equals(operate)) {
			pageFields.add(field_note);
		}
		else if(OP_ASSIGN.equals(operate)) {
			pageFields.add(field_modifier);
			field_modifier.setRequired(true);
		}
		else if(OP_GOTEST.equals(operate)) {
			pageFields.add(field_testorId);
			pageFields.add(field_solveinfo);
			pageFields.add(field_relatetest);
			field_testorId.setRequired(true);
		}
		else if(OP_CHGTESTOR.equals(operate)) {
			pageFields.add(field_testorId);
			field_testorId.setRequired(true);
		}
		else if(OP_FAILURE.equals(operate)) {
			pageFields.add(field_note);
			pageFields.add(field_intro);
			pageFields.add(field_reappear);
		}
		
		return pageFields;
	}
	
	/**
	 * combine bug operate page html
	 * 
	 * @param operate
	 * @param bug
	 * @return
	 */
	public static String combineBtnHtml(String operate, Bug bug, String workspace) {
		
		StringBuffer html = new StringBuffer();
		int status = 0;
		
		String returnPage = Constants.VALID_YES.equals(workspace) ? "work/openWork.do" : "pages/bug/bug.jsp";
		returnPage = Constants.BASEPATH + returnPage;
		
		// create operate
		if(OP_CREATE.equals(operate)) {
			html.append("<input type='submit' class='btn button' value='保存' onclick='setStatus(" + Constants.BUG_STATUS_INIT + ");' />");
			html.append("<input type='submit' class='btn button' value='提交' onclick='setStatus(" + Constants.BUG_STATUS_AUDIT + ");' />");
			html.append("<a href='" + returnPage + "' class='btn button'>返回</a>");
		}
		// edit operate
		else if(OP_EDIT.equals(operate)) {
			html.append("<input type='submit' class='btn button' value='保存' onclick='setStatus(" + Constants.BUG_STATUS_INIT + ");' />");
			html.append("<a href='" + Constants.BASEPATH + "/bug/openBug.do?bugId=" + bug.getId() + "' class='btn button'>返回</a>");
		}
		// commit operate
		else if(OP_COMMIT.equals(operate)) {
			html.append("<input type='submit' class='btn button' value='提交' onclick='setStatus(" + Constants.BUG_STATUS_AUDIT + ");' />");
			html.append("<a href='" + Constants.BASEPATH + "/bug/openBug.do?bugId=" + bug.getId() + "' class='btn button'>返回</a>");
		}
		// other operate
		else {
			if(OP_UNPASS.equals(operate)) {
				status = Constants.BUG_STATUS_INIT;
			} else if(OP_HANGUP.equals(operate)) {
				status = Constants.BUG_STATUS_HANGUP;
			} else if(OP_CLOSE.equals(operate) || OP_SUCCESS.equals(operate)) {
				status = Constants.BUG_STATUS_CLOSE;
			} else if(OP_ASSIGN.equals(operate) || OP_FAILURE.equals(operate)) {
				status = Constants.BUG_STATUS_SOLVE;
			} else if(OP_GOTEST.equals(operate) || OP_CHGTESTOR.equals(operate)) {
				status = Constants.BUG_STATUS_TEST;
			} else if(OP_REOPEN.equals(operate)) {
				status = Constants.BUG_STATUS_AUDIT;
			}
			
			html.append("<input type='submit' class='btn button' value='确定' onclick='setStatus(" + status + ");' />");
			html.append("<a href='" + Constants.BASEPATH + "/bug/openBug.do?bugId=" + bug.getId() + "' class='btn button'>取消</a>");
		}
		
		return html.toString();
	}
}
