package com.cloud.project.web;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloud.bug.service.BugStatService;
import com.cloud.bug.vo.BugSearchVo;
import com.cloud.platform.Constants;
import com.cloud.platform.DateUtil;
import com.cloud.project.model.Project;
import com.cloud.project.service.ProjectService;

@Controller
@RequestMapping("project")
public class ProjectBean {

	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private BugStatService bugStatService;
	
	/**
	 * add project info before create or edit project
	 * 
	 * @param projectId
	 * @return
	 */
	@RequestMapping("/createOrEdit.do")
	public ModelAndView createOrEdit(
			@RequestParam(value = "projectId", required = false) String projectId) {
		
		ModelAndView mv = new ModelAndView("project/projectAdd");
		
		Project project = projectService.getProject(projectId);
		mv.addObject("project", project);
		
		return mv;
	}
	
	/**
	 * open project
	 * 
	 * @param projectId
	 * @return
	 */
	@RequestMapping("/openProject.do")
	public ModelAndView openProject(@RequestParam("projectId") String projectId) {
		
		ModelAndView mv = new ModelAndView("project/projectLook");
		
		try {
			// get bug info
			Project project = projectService.getProject(projectId);
			
			// get bug status statistic info
			JSONArray statusStat = bugStatService.statBugStatus(projectId, false);
			
			// get person recive bug statistic info
			JSONArray personReciveStat = bugStatService.statPersonRecive(projectId);
			
			// get person solve bug statistic info
			JSONArray personSolveStat = bugStatService.statPersonSolve(projectId);
			
			// if login user is project manager
			String canEdit = Constants.getLoginUserId().equals(
					project.getManagerId()) ? Constants.VALID_YES : Constants.VALID_NO;
			
			// init model and view
			mv.addObject("project", project);
			mv.addObject("statusStat", statusStat);
			mv.addObject("personReciveStat", personReciveStat);
			mv.addObject("personSolveStat", personSolveStat);
			mv.addObject("canEdit", canEdit);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return mv;
	}
	
	/**
	 * get projects
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getProjects.do")
	public String getProjects(BugSearchVo searchVo) {
		
		// get projects
		List<Project> projects = projectService.searchProjects(searchVo);
		
		// convert to json format
		JSONObject info = new JSONObject();
		info.put("page", searchVo);
		info.put("projects", projects);
		
		return info.toString();
	}
	
	/**
	 * save project 
	 * 
	 * @param project
	 * @return
	 */
	@RequestMapping("/saveProject.do")
	public String saveProject(Project project,
			@RequestParam("start") String startDate, @RequestParam("end") String endDate) {
		
		try {
			project.setStartDate(DateUtil.parseDate(startDate));
			project.setEndDate(DateUtil.parseDate(endDate));
			
			projectService.saveProject(project);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return "project/project";
	}
}
